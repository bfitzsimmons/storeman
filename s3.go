package storeman

import (
	"bitbucket.org/bfitzsimmons/caller"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/service/s3"
)

/*----------------------------------------------------------------------------
Amazon S3
----------------------------------------------------------------------------*/

// S3 wraps the S3 client so that functionality may be added if needed.
type S3 struct {
	*s3.S3
}

// InitializeS3 initializes the connection to S3.
func InitializeS3(accessKey, secretKey string, HTTPClient *caller.HTTPClient) *S3 {
	config := &aws.Config{
		Credentials: credentials.NewStaticCredentials(accessKey, secretKey, ""),
		Region:      aws.String("us-east-1"),
		MaxRetries:  aws.Int(15),
		HTTPClient:  HTTPClient.Client,
	}
	connection := s3.New(config)
	return &S3{connection}
}
